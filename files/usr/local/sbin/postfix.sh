#!/usr/bin/env bash

set -Eeuo pipefail

function signalPostfix() {
  SIGNAL="$1"
  MASTER_PID_FILE="$(postconf -h queue_directory)/$(postconf -h process_id_directory)/master.pid"

  if [[ ! -f "$MASTER_PID_FILE" ]]; then
    echo "Cannot locate the master.pid file" >/dev/stderr
    return
  fi

  MASTER_PID_UNTRIMMED="$(cat -- "$MASTER_PID_FILE")"
  MASTER_PID="${MASTER_PID_UNTRIMMED##* }"

  kill "-$SIGNAL" "$MASTER_PID" 2>/dev/null
}

function shutdownPostfix() {
  signalPostfix INT
}

POSTFIX_PID=0

trap 'signalPostfix INT; wait "$POSTFIX_PID"' SIGINT

/usr/sbin/postfix start-fg &

POSTFIX_PID=$!

wait "$POSTFIX_PID"
