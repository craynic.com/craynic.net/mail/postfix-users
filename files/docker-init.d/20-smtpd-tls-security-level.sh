#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_SMTP_SECURITY_LEVEL" ]]; then
  exit 0
fi

update-postfix-config.sh <(echo "smtpd_tls_security_level = $POSTFIX_SMTP_SECURITY_LEVEL")
